import React, { useEffect, useState } from "react";
import WeatherNow from './components/WeatherNow';
import WeatherForecast from "./components/WeatherForecast";
export default function App() {

  const [lat, setLat] = useState([]);
  const [long, setLong] = useState([]);
  const [weatherDataNow, setWeatherDataNow] = useState([]);
  const [weatherDataForecast5firstHours, setWeatherDataForecast5firstHours] = useState([]);
  const [weatherDataForecast5Days, setWeatherDataForecast5Days] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    // Get geolocalisation if user has accepted in navigator
    navigator.geolocation.getCurrentPosition(function(position) {
      setLat(position.coords.latitude);
      setLong(position.coords.longitude);
    });

    const getWeather = (lat, long, type) => {
      if (type === 'now') {
        return fetch(`${process.env.REACT_APP_API_URL}/weather?lat=${lat}&lon=${long}&appid=${process.env.REACT_APP_API_KEY}`)
          .then(res => handleResponse(res))
          .then(weather => {
            if (Object.entries(weather).length) {
              return weather;
            }
          });
      }
      if(type === 'forecast5firstHours') {
        return fetch(`${process.env.REACT_APP_API_URL}/forecast?lat=${lat}&lon=${long}&cnt=5&appid=${process.env.REACT_APP_API_KEY}`)
          .then(res => handleResponse(res))
          .then(weather => {
            if (Object.entries(weather).length) {
              return weather;
            }
          });
      }
      if(type === 'forecast5days') {
        return fetch(`${process.env.REACT_APP_API_URL}/forecast/?lat=${lat}&lon=${long}&appid=${process.env.REACT_APP_API_KEY}`)
          .then(res => handleResponse(res))
          .then(weather => {
            if (Object.entries(weather).length) {
              return weather;
            }
          });
      }
    }


    getWeather(lat, long, 'now')
      .then(weather => {
        setWeatherDataNow(weather);
        setError(null);
      })
      .catch(err => {
        setError(err.message);
      });

    getWeather(lat, long, 'forecast5firstHours')
      .then(weathers => {
        setWeatherDataForecast5firstHours(weathers.list);
        setError(null);
      })
      .catch(err => {
        setError(err.message);
      });

    getWeather(lat, long, 'forecast5days')
      .then(weathers => {
        setWeatherDataForecast5Days(weathers.list);
        setError(null);
      })
      .catch(err => {
        setError(err.message);
      });

  }, [lat,long,error])

  const handleResponse = (response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error("Please Enable your Location in your browser!");
    }
  }


  return (
    <div className="App">
      {(typeof weatherDataNow.main != 'undefined') ? (
          <div className="flex flex-col items-center justify-center w-screen min-h-screen text-gray-700 p-10 bg-gradient-to-br from-pink-200 via-purple-200 to-indigo-200 ">
            <WeatherNow weatherData={weatherDataNow} weatherDataTodayForecast={weatherDataForecast5firstHours}/>
            <WeatherForecast weatherData={weatherDataForecast5Days}/>
          </div>
      ): (
        <div>
          <p>LOAD</p>
        </div>
      )}
    </div>
  );
}
