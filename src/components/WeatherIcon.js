export default function WeatherIcon({mainWeather, className, icon}) {
  return (
    <img src={`https://openweathermap.org/img/wn/${icon}@2x.png`} className={className} alt={`icon of ${mainWeather}`}/>
  );
}
