import React from "react";
import moment from 'moment';
import WeatherIcon from "./WeatherIcon";

export default function WeatherForecast({weatherData}) {

  function groupByDay() {
    const tmpGroupedData = new Map();

    for (const objet of weatherData) {
      const date = objet.dt_txt.slice(0, 10); // Extrayez la date sous forme de chaîne (par exemple, "2023-11-03").

      if (tmpGroupedData.has(date)) {
        tmpGroupedData.get(date).push(objet); // Ajoutez l'objet au tableau existant pour cette date.
      } else {
        tmpGroupedData.set(date, [objet]); // Créez une nouvelle entrée dans la Map pour cette date.
      }
    }

    // Convertissez la Map en un tableau (si nécessaire).
    return [...tmpGroupedData];
  }

  const forecastGrouped = groupByDay();


  return (
    <div className="flex flex-col space-y-6 w-full max-w-screen-sm bg-white p-10 mt-10 rounded-xl ring-8 ring-white ring-opacity-40">
      {forecastGrouped.map(weather => (
        <details className={'cursor-pointer'}>
          <summary className={'font-bold text-3xl'}>{moment(weather[0]).format('dddd d MMMM')}</summary>
          {weather[1].map(weatherHourly => (
            <div className="flex justify-between items-center" >
              <span className="font-semibold text-lg w-1/4">{moment(weatherHourly.dt_txt).format('hh:00 a')}</span>
              <div className="flex items-center justify-end w-1/4 pr-10">
                <span className="font-semibold">{weatherHourly.main.humidity}%</span>
                <svg className={'h-6 w-6'} xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 384 512"><path d="M192 512C86 512 0 426 0 320C0 228.8 130.2 57.7 166.6 11.7C172.6 4.2 181.5 0 191.1 0h1.8c9.6 0 18.5 4.2 24.5 11.7C253.8 57.7 384 228.8 384 320c0 106-86 192-192 192zM96 336c0-8.8-7.2-16-16-16s-16 7.2-16 16c0 61.9 50.1 112 112 112c8.8 0 16-7.2 16-16s-7.2-16-16-16c-44.2 0-80-35.8-80-80z"/></svg>
              </div>
              <WeatherIcon className={'h-12 w-12'} mainWeather={weatherHourly.weather[0].main} icon={weatherHourly.weather[0].icon}></WeatherIcon>
              <span className="font-semibold text-lg w-1/4 text-right">{Math.round((weatherHourly.main.temp_min / 32) * 10) / 10}°C / {Math.round((weatherHourly.main.temp_max / 32) * 10) / 10}°</span>
            </div>
          ))}
        </details>
      ))}
      </div>
  )
}
