import React from 'react';
import moment from 'moment';

import WeatherIcon from "./WeatherIcon";

export default function weatherNow({weatherData, weatherDataTodayForecast}) {
  return (
    <div className="w-full max-w-screen-sm bg-white p-10 rounded-xl ring-8 ring-white ring-opacity-40">
          <div className="flex justify-between">
            <div className="flex flex-col">
              <span className="text-6xl font-bold">{Math.round((weatherData.main.temp / 32) * 10) / 10}°C</span>
              <span className="text-2xl font-semibold mt-1 text-gray-500 text-left">{weatherData.name}</span>
            </div>
            <WeatherIcon className={'h-24 w-24'} mainWeather={weatherData.weather[0].main} icon={weatherData.weather[0].icon}></WeatherIcon>
          </div>
          <div>
            <p className="text-xs font-bold text-gray-500">Levé: {new Date(weatherData.sys.sunrise * 1000).toLocaleTimeString('en-IN')}</p>
            <p className="text-xs font-bold text-gray-500">Couché: {new Date(weatherData.sys.sunset * 1000).toLocaleTimeString('en-IN')}</p>
          </div>
          <div className="flex justify-between mt-12">
            {weatherDataTodayForecast.map(weather => (
              <div className="flex flex-col items-center">
              <span className="font-semibold text-lg">{Math.round((weather.main.temp / 32) * 10) / 10}°C</span>
                <WeatherIcon className={'h-10 w-10'} mainWeather={weather.weather[0].main} icon={weather.weather[0].icon}></WeatherIcon>
                <span className="font-semibold mt-1 text-sm">{moment(weather.dt_txt).format("hh:mm")}</span>
                <span className="text-xs font-semibold text-gray-400">{moment(weather.dt_txt).format("a")}</span>
              </div>
            ))}
          </div>
        </div>
  )
}
